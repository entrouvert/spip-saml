<?php

/* Loading simplesamlphp */
include_spip('inc/simplesamlphp/lib/_autoload');

include_spip('inc/headers');
include_spip('inc/session');

include_spip('inc/my_auth');

function saml_definir_session($session)
{
  // Vérification de la connection à l'idp
  if (verifier_session())
  {
    $simplesaml = new SimpleSAML_Auth_Simple('default-sp');
    if (!$simplesaml->isAuthenticated())
    {
        my_spip_logout();
        redirige_par_entete('/');
    }
  }
  return $session;
}

?>
