<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2007                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/cookie');
include_spip('inc/my_auth');

/* Loading simplesamlphp */
include_spip('inc/simplesamlphp/lib/_autoload');

// http://doc.spip.org/@action_logout_dist
function action_logout_dist()
{
    $simplesaml = new SimpleSAML_Auth_Simple('default-sp');
    // Spip logout
    my_spip_logout();

    // Logout SAML
    if ($simplesaml->isAuthenticated())
    {
        spip_log("[auth_saml] logout " . url_de_base());
        $simplesaml->logout(url_de_base());
    }
}
?>
