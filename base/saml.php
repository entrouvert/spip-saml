<?php
/**
 * Plugin SAML
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Ajouter des champs a la table auteurs
 * @param array $tables_principales
 * @return array
 */
function saml_declarer_tables_principales(&$tables_principales) {
        // Extension de la table auteurs
        $tables_principales['spip_auteurs']['field']['nameid'] = "text DEFAULT '' NOT NULL";
        
        return $tables_principales;
}

?>
