<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/meta');
include_spip('base/create');

function saml_install($action) {
    $plugins_actifs = liste_plugin_actifs();
    $version_script = $plugins_actifs['SAML']['version'];

    switch ($action) {
        case 'test':
            return ((isset($GLOBALS['meta']['saml_version'])) AND 
                ($GLOBALS['meta']['saml_version'] == $version_script));
        case 'install':
            include_spip('base/saml');
            maj_tables('spip_auteurs');
            ecrire_meta('saml_version', $version_script);
            ecrire_metas();
            break;
    }
}

?>
