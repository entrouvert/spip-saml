<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/cookie');

function my_spip_logout()
{
    global $auteur_session;

    spip_log("[auth_saml] logout from spip");
    $logout =_request('logout');
    $url = _request('url');
    spip_log("logout $logout $url" . $auteur_session['id_auteur']);
    // cas particulier, logout dans l'espace public
    if ($logout == 'public' AND !$url)
        $url = url_de_base();

    // seul le loge peut se deloger (mais id_auteur peut valoir 0 apres une restauration avortee)
    if (is_numeric($auteur_session['id_auteur'])) {
        spip_query("UPDATE spip_auteurs SET en_ligne = DATE_SUB(NOW(),INTERVAL 15 MINUTE) WHERE id_auteur = ".$auteur_session['id_auteur']);
    // le logout explicite vaut destruction de toutes les sessions
        if ($_COOKIE['spip_session']) {
            $session = charger_fonction('session', 'inc');
            $session($auteur_session['id_auteur']);
            preg_match(',^[^/]*//[^/]*(.*)/$,',
                   url_de_base(),
                   $r);
            spip_setcookie('spip_session', '', -1,$r[1]);
            spip_setcookie('spip_session', '', -1);
            spip_setcookie('spip_admin', '', -1, $r[1]);
            spip_setcookie('spip_admin', '', -1);
        }
        if ($_SERVER['PHP_AUTH_USER']) {
            include_spip('inc/actions');
            if (verifier_php_auth()) {
              ask_php_auth(_T('login_deconnexion_ok'),
                       _T('login_verifiez_navigateur'),
                       _T('login_retour_public'),
                           "redirect=". _DIR_RESTREINT_ABS, 
                       _T('login_test_navigateur'),
                       true);
              exit;
            }
        }
    }
}

?>
