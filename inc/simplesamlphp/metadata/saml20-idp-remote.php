<?php

$metadata['https://connexion.meyzieu.fr/idp/saml2/metadata'] = array (
  'entityid' => 'https://connexion.meyzieu.fr/idp/saml2/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion.meyzieu.fr/idp/saml2/sso',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion.meyzieu.fr/idp/saml2/sso',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion.meyzieu.fr/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion.meyzieu.fr/idp/saml2/slo_return',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion.meyzieu.fr/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion.meyzieu.fr/idp/saml2/slo_return',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion.meyzieu.fr/idp/saml2/slo/soap',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion.meyzieu.fr/idp/saml2/artifact',
      'index' => 0,
    ),
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIICDDCCAXWgAwIBAgIJAJ2Y9+QwMm5aMA0GCSqGSIb3DQEBBQUAMB8xHTAbBgNV
BAMMFGNvbm5leGlvbi5tZXl6aWV1LmZyMB4XDTE2MDMyMTA5MzY1M1oXDTI2MDMy
MTA5MzY1M1owHzEdMBsGA1UEAwwUY29ubmV4aW9uLm1leXppZXUuZnIwgZ8wDQYJ
KoZIhvcNAQEBBQADgY0AMIGJAoGBAL6JHjQR4lWFANPWXMsroRYgc+urWNeFq1dE
nRKYqRzbXZcNjhW4+wMDsjPAnr9xS2tnd6Re3Bga2EpWqIRg/7JriRwWzmweTDTL
7EuN2UdvEUAbUcIWYDH4YcOG95YrDwGaMz3Zm+VZicEm4G2XRzVXj2KgyF6zhY1G
y0Yt78d9AgMBAAGjUDBOMB0GA1UdDgQWBBRGV/fuK//pc1ljnWs66XWpcxsmHzAf
BgNVHSMEGDAWgBRGV/fuK//pc1ljnWs66XWpcxsmHzAMBgNVHRMEBTADAQH/MA0G
CSqGSIb3DQEBBQUAA4GBADVJ8NmRbQEpFj4tN/VjTFvaSR0FEDyUfeSWMbf0PqLp
kqIcDpOxOTFOaqZHFLdwgvIr1YUxRjvASqtOr4JiaT7kGtKqJcSHGzhOAQIZmU6L
YCtPbe+N04mMPYS/RDAKJTr3Oy6YidVOfQbWEcvqRIV4Bhp+P5ujSewQm8PqL0Lt',
    ),
  ),
);

$metadata['https://connexion-meyzieu.test.entrouvert.org/idp/saml2/metadata'] = array (
  'entityid' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/sso',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/sso',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/slo_return',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/slo_return',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/slo/soap',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion-meyzieu.test.entrouvert.org/idp/saml2/artifact',
      'index' => 0,
    ),
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIICLjCCAZegAwIBAgIJALJ2zWF1DvjfMA0GCSqGSIb3DQEBBQUAMDAxLjAsBgNV
BAMMJWNvbm5leGlvbi1tZXl6aWV1LnRlc3QuZW50cm91dmVydC5vcmcwHhcNMTYw
MTI3MjIxNjE4WhcNMjYwMTI2MjIxNjE4WjAwMS4wLAYDVQQDDCVjb25uZXhpb24t
bWV5emlldS50ZXN0LmVudHJvdXZlcnQub3JnMIGfMA0GCSqGSIb3DQEBAQUAA4GN
ADCBiQKBgQDOae7W49Jd4JzUUz5ZoG8dj4hlGqdb65O1Pg2BrsvmKIa4R+EbFhhl
+H/CYjHsB8eaeNzXWgQaqVweLImY4Viw19EGgfG/Hjg1Qo40Yynrk5qQVHgcVbXh
TqGizA5m4rBQn/WYcLEQ41ZV2IMLNR+0ux2WAcHn48jqvLjdTdsTjQIDAQABo1Aw
TjAdBgNVHQ4EFgQUMB+aDBOwiwWnf0nxDl8Xx5oo+1owHwYDVR0jBBgwFoAUMB+a
DBOwiwWnf0nxDl8Xx5oo+1owDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOB
gQA+zTdThAHHDHmxa3/Ys5HtPy6lXP5HxT6QRgaiQDzqM7OzYtvtO0t0HFN6D+aX
Nt+Y/3N1sHbwT4qMVETD0+AfBXxSXu4ZmFMmEt4gY4gEjEnocqKs6lm0MoMoyqhH
c9I3aCM8xLrfDjgLd1GopSH7cVlOjHuTFTT1s0z53cOz7A==',
    ),
  ),
);

$metadata['https://mon-meyzieu.dev.entrouvert.org/idp/saml2/metadata'] = array (
  'entityid' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/sso',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/sso',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/slo',
      'ResponseLocation' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/slo_return',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/slo',
      'ResponseLocation' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/slo_return',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/slo/soap',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://mon-meyzieu.dev.entrouvert.org/idp/saml2/artifact',
      'index' => 0,
    ),
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDIzCCAgugAwIBAgIJANUBoick1pDpMA0GCSqGSIb3DQEBBQUAMBUxEzARBgNV
BAoTCkVudHJvdXZlcnQwHhcNMTAxMjE0MTUzMzAyWhcNMTEwMTEzMTUzMzAyWjAV
MRMwEQYDVQQKEwpFbnRyb3V2ZXJ0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
CgKCAQEAvxFkfPdndlGgQPDZgFGXbrNAc/79PULZBuNdWFHDD9P5hNhZn9Kqm4Cp
06Pe/A6u+g5wLnYvbZQcFCgfQAEzziJtb3J55OOlB7iMEI/T2AX2WzrUH8QT8NGh
ABONKU2Gg4XiyeXNhH5R7zdHlUwcWq3ZwNbtbY0TVc+n665EbrfV/59xihSqsoFr
kmBLH0CoepUXtAzA7WDYn8AzusIuMx3n8844pJwgxhTB7Gjuboptlz9Hri8JRdXi
VT9OS9Wt69ubcNoM6zuKASmtm48UuGnhj8v6XwvbjKZrL9kA+xf8ziazZfvvw/VG
Tm+IVFYB7d1x457jY5zjjXJvNysoowIDAQABo3YwdDAdBgNVHQ4EFgQUeF8ePnu0
fcAK50iBQDgAhHkOu8kwRQYDVR0jBD4wPIAUeF8ePnu0fcAK50iBQDgAhHkOu8mh
GaQXMBUxEzARBgNVBAoTCkVudHJvdXZlcnSCCQDVAaInJNaQ6TAMBgNVHRMEBTAD
AQH/MA0GCSqGSIb3DQEBBQUAA4IBAQAy8l3GhUtpPHx0FxzbRHVaaUSgMwYKGPhE
IdGhqekKUJIx8et4xpEMFBl5XQjBNq/mp5vO3SPb2h2PVSks7xWnG3cvEkqJSOeo
fEEhkqnM45b2MH1S5uxp4i8UilPG6kmQiXU2rEUBdRk9xnRWos7epVivTSIv1Ncp
lG6l41SXp6YgIb2ToT+rOKdIGIQuGDlzeR88fDxWEU0vEujZv/v1PE1YOV0xKjTT
JumlBc6IViKhJeo1wiBBrVRIIkKKevHKQzteK8pWm9CYWculxT26TZ4VWzGbo06j
o2zbumirrLLqnt1gmBDvDvlOwC/zAAyL4chbz66eQHTiIYZZvYgy',
    ),
  ),
);

